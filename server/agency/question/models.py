from django.db import models
from django.core.exceptions import ValidationError

TEXT = 'T'
FILE = 'F'
QUESTION_TYPES = (
    (TEXT, 'Text'),
    (FILE, 'File'),
)

class Question(models.Model):
    question_text = models.TextField()
    question_type = models.CharField(
        max_length=2,
        choices=QUESTION_TYPES,
        default=TEXT,
    )

    def __str__(self):
        return str(self.question_text)


class Response(models.Model):
    question = models.ForeignKey(Question, on_delete=models.PROTECT)
    response_text = models.TextField(null=True)
    response_url = models.TextField(null=True)

    def clean(self):
        if self.response_url is None and self.response_text is None:
            raise ValidationError('Response needs to have either a text response or a file upload!')

    def __str__(self):
        return str(self.pk)
