from django.apps import AppConfig


class ClientspaceConfig(AppConfig):
    name = 'clientspace'
