from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=100)
    duration_days = models.IntegerField(default=1)

    def __str__(self):
        return self.name
